
const express = require("express")
const cookieParser = require("cookie-parser")
const uuidv4 = require("uuid/v4")
const mongodb = require("mongodb")

const MongoClient = mongodb.MongoClient

function buildURL(url, obj) {
  const params = new URLSearchParams()
  for (const key of Object.keys(obj)) {
    params.set(key, obj[key])
  }
  return url + "?" + params.toString()
}

;(async function main () {

  const app = express()

  const client = await MongoClient.connect("mongodb://fyp.3warriors.tk/data0", {
    auth: {user: "developer", password: "developer"}
  })
  const db = client.db()

  app.set("view engine", "ejs")

  app.use(express.urlencoded())
  app.use(cookieParser())

  app.get("/", async (req, res) => {
    const lists = await db.collection("todolist")
      .find({}, {projection: {title: 1}})
      .toArray()
    res.render("index.ejs", {lists})
  })

  app.get("/view-list", async (req, res, next) => {
    const {id} = req.query
    const doc = await db.collection("todolist").findOne({_id: id})
    if (doc) {
      res.render("view-list.ejs", doc)
    } else {
      next()
    }
  })

  app.post("/add-item", async (req, res) => {
    const {id, content} = req.body

    const entry = {content, completed: false}
    await db.collection("todolist").update({_id: id}, {$push: {items: entry}})
    res.redirect(buildURL("/view-list", {id}))
  })

  app.post("/delete-item", async (req, res) => {
    const {id, index} = req.body

    await db.collection("todolist").update({_id: id}, {$unset: {[`items.${index}`]: 1}})
    await db.collection("todolist").update({_id: id}, {$pull: {items: null}})
    res.redirect(buildURL("/view-list", {id}))
  })

  app.post("/toggle-item", async (req, res) => {
    const {id, index} = req.body

    const doc = await db.collection("todolist").findOne({_id: id})
    const newValue = !doc.items[index].completed

    await db.collection("todolist").update({_id: id}, {$set: {[`items.${index}.completed`]: newValue}})
    res.redirect(buildURL("/view-list", {id}))
  })

  app.post("/change-item-title", async (req, res) => {
    const {id, title} = req.body

    await db.collection("todolist").update({_id: id}, {$set: {title}})
    res.redirect(buildURL("/view-list", {id}))
  })

  app.post("/new-list", async (req, res) => {
    const id = uuidv4()
    await db.collection("todolist").insert({_id: id, title: "Untitled", items: []})
    res.redirect(buildURL("/view-list", {id}))
  })

  app.listen(8900)

})()
